import {
    // GetBaseUoM,
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {
    NutrientFact,
    Product,
    SupplierProduct
} from "./supporting-files/models";
import {
    // ConvertUnits,
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
    SumUnitsOfMeasure
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/*
 * Get Cheapest supplier product from a list of supplier products
 * by calculating the cost per base unit
 */
const GetCheapestSupplierProduct = (
    supplierProducts: SupplierProduct[]
): SupplierProduct => {
    return supplierProducts.reduce(
        (cheapestSupplierProduct, supplierProduct) => {
            const costA = GetCostPerBaseUnit(cheapestSupplierProduct);
            const costB = GetCostPerBaseUnit(supplierProduct);

            return costA < costB ? cheapestSupplierProduct : supplierProduct;
        },
        supplierProducts[0]
    );
};

/*
 * Get Cheapest product from a list of products
 * by calculating the cost per base unit of the cheapest supplier product
 */
const GetCheapestProduct = (products: Product[]): Product => {
    return products.reduce((cheapestProduct, product) => {
        const costA = GetCostPerBaseUnit(
            GetCheapestSupplierProduct(cheapestProduct.supplierProducts)
        );
        const costB = GetCostPerBaseUnit(
            GetCheapestSupplierProduct(product.supplierProducts)
        );

        return costA < costB ? cheapestProduct : product;
    }, products[0]);
};

recipeData.forEach((recipe) => {
    let cheapestCost = 0;
    const nutrientsAtCheapestCost = {};

    recipe.lineItems.forEach((recipeLineItem) => {
        const productsOfIngredient = GetProductsForIngredient(
            recipeLineItem.ingredient
        );

        // Get the cheapest supplier product and get the its cost per base unit
        const cheapestProduct = GetCheapestProduct(productsOfIngredient);
        const cheapestSupplierProduct = GetCheapestSupplierProduct(
            cheapestProduct.supplierProducts
        );
        const costPerBaseUnit = GetCostPerBaseUnit(cheapestSupplierProduct);

        // // Get the recipe's uom by converting its current to its base
        // const recipeBaseUom = GetBaseUoM(recipeLineItem.unitOfMeasure.uomType);
        // const recipeConvertedUom = ConvertUnits(
        //     recipeLineItem.unitOfMeasure,
        //     recipeBaseUom.uomName,
        //     recipeBaseUom.uomType
        // );

        // // Get the supplier product's uom by converting its current to its base
        // const supplierProductBaseUom = GetBaseUoM(
        //     cheapestSupplierProduct.supplierProductUoM.uomType
        // );
        // const supplierProductConvertedUom = ConvertUnits(
        //     cheapestSupplierProduct.supplierProductUoM,
        //     supplierProductBaseUom.uomName,
        //     supplierProductBaseUom.uomType
        // );

        // // Get the uom to be used by converting the recipes base oum to the supplier product's base ourm
        // // e.g. if recipe is in cups/volume but supplier is in grams/mass
        // const uom = ConvertUnits(
        //     recipeConvertedUom,
        //     supplierProductConvertedUom.uomName,
        //     supplierProductConvertedUom.uomType
        // );
        // cheapestCost += uom.uomAmount * costPerBaseUnit;
        cheapestCost +=
            recipeLineItem.unitOfMeasure.uomAmount * costPerBaseUnit;

        cheapestProduct.nutrientFacts.forEach((nutrientFact) => {
            const nutrientFactBase = GetNutrientFactInBaseUnits(nutrientFact);
            if (nutrientsAtCheapestCost[nutrientFact.nutrientName]) {
                const currentNutrientFact: NutrientFact =
                    nutrientsAtCheapestCost[nutrientFact.nutrientName];
                nutrientsAtCheapestCost[nutrientFact.nutrientName] = {
                    nutrientName: nutrientFactBase.nutrientName,
                    // Calculate the sum of current nutrient fact and the base nutrient fact
                    quantityAmount: SumUnitsOfMeasure(
                        currentNutrientFact.quantityAmount,
                        nutrientFactBase.quantityAmount
                    ),
                    quantityPer: nutrientFactBase.quantityPer
                };
            } else {
                nutrientsAtCheapestCost[nutrientFact.nutrientName] =
                    nutrientFactBase;
            }
        });
    });

    recipeSummary[recipe.recipeName] = {
        // cheapestCost: 2.2291416666666666,
        cheapestCost,
        nutrientsAtCheapestCost: Object.keys(nutrientsAtCheapestCost) // sort nutrient fact by nutrient name
            .sort()
            .reduce((obj, key) => {
                obj[key] = nutrientsAtCheapestCost[key];
                return obj;
            }, {})
    };
});
console.log("Current Result Is:", recipeSummary);

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
